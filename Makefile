ifeq ($(PREFIX),)
	PREFIX := /usr/local
endif

install:
	mkdir -p $(PREFIX)/bin
	install -Dm744 -t $(PREFIX)/bin lsb_release
